@extends('layouts.master')
@section('content')
<div class="container my-2">
  @auth
  <a href="/berita/create" class="btn btn-primary my-2">Tambah Berita</a>
  <table class="table">
      <thead class="thead-light">
          <tr>
              <th scope="col">No</th>
              <th scope="col">Judul Berita</th>
              <th scope="col">Kategori</th>
              <th scope="col">Penulis</th>
              <th scope="col">Aksi</th>
            </tr>
      </thead>
      <tbody>
          @forelse ($berita as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
                <td>{{$value->judul}}</td>
                  <td>{{$value->kategori->nama}}</td>
                  <td>{{$value->user->name}}</td>
                  <td>
                      @if ($value->user->id === Auth::user()->id)
                    <form action="/berita/{{$value->id}}" method="POST">
                        @csrf
                      @method('DELETE')
                      <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a>
                      <a href="/berita/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                      <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                      @else
                      <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a> 
                        @endif
                        
                  </td>
                </tr>
                      @empty
                      <tr colspan="3">
                          <td>No data</td>
                        </tr>  
                      @endforelse              
                  </tbody>
                </table>
          </div>
          @endauth
          @guest
          <h1>Selamat Datang di portal berita kami </h1>
    <hr>
    <div class="row">
            @forelse ($berita as $value)
            <div class="card mx-3" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">{{$value->judul}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">Penulis {{$value->user->name}}</h6>
                  <h6 class="card-subtitle mb-2 text-muted">Kategori : {{$value->kategori->nama}}</h6>
                  <p class="card-text">{{$value->isi}}</p>
                  <small>Waktu Posting {{$value->created_at}} </small><br>
                  @guest
                  <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a> 
                  @endguest
                </div>
              </div>
              @empty
              <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <p class="card-text">Belum Ada Berita</p>
                </div>
            </div>
            @endforelse
    </div>
          @endguest
@endsection