@extends('layouts.master')
@section('content')
<div class="container my-2">
    <h2>Show Kategori {{$kategori->id}}</h2>
    <h4>{{$kategori->nama}}</h4>

    <a href="/kategori" class="btn btn-primary">Kembali</a>
</div>

@endsection