@extends('layouts.master')
@section('content')
    <div class="container my-2">
<a href="{{ route('kategori.create') }}" class="btn btn-primary mb-3">Tambah Kategori</a>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Kategori</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="{{ route ('kategori.destroy', ['kategori'=>$value->id])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="{{ route ('kategori.show', ['kategori'=> $value->id])}}" class="btn btn-info">Show</a>
                        <a href="{{ route ('kategori.edit', ['kategori'=> $value->id])}}" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
                    </tr>
                    @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                    @endforelse              
                </tbody>
            </table>
        </div>
@endsection