@extends('layouts.master')
@section('content')
<form action="/profile/{{$profil->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="{{$profil->user->email}}" id="email" disabled>
    </div>
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" name="name" value="{{$profil->user->name}}" id="name" disabled>
    </div>
    <div class="form-group">
        <label for="age">Umur</label>
        <input type="number" class="form-control" name="age" value="{{$profil->age}}" id="age">
        @error('age')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="address">Alamat</label>
        <textarea name="address" id="address" class="form-control" cols="30" rows="10">{{$profil->address}}</textarea>
        @error('address')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection