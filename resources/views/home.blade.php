@extends('layouts.master')
@section('content')
    <h1>Selamat Datang di portal berita kami </h1>
    <hr>
    <div class="row">
            @forelse ($berita as $value)
            <div class="card mx-3" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">{{$value->judul}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">Penulis {{$value->user->name}}</h6>
                  <h6 class="card-subtitle mb-2 text-muted">Kategori : {{$value->kategori->nama}}</h6>
                  <p class="card-text">{{$value->isi}}</p>
                  <small>Waktu Posting {{$value->created_at}} </small><br>
                  @auth
                  @if ($value->user->id === Auth::user()->id)
                  <form action="/berita/{{$value->id}}" method="POST">
                    <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a>
                    <a href="/berita/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                          @csrf
                          @method('DELETE')
                          <input type="submit" class="btn btn-danger my-1" value="Delete">
                      </form>
                    @else
                        <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a> 
                      @endif
                  @endauth
                  @guest
                  <a href="/berita/{{$value->id}}" class="btn btn-primary">Read More</a> 
                  @endguest
                </div>
              </div>
              @empty
              <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <p class="card-text">Belum Ada Berita</p>
                </div>
            </div>
            @endforelse
    </div>
@endsection
@auth
    @push('scripts')
    <script src="{{asset('/sbadmin2/swal.min.js')}}"></script>
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Anda Berhasil Login",
            icon: "success",
            confirmButtonText: "Cool",
        });
    </script>
    @endpush
@endauth